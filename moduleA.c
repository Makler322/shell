#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <pwd.h>
#include "module.h"

extern LIST *listMain;
extern BUF *bufMain;
extern int c, countBG;

int testCD(){
    int flagCD = 0;
    if (listMain -> sizelist == 0){
        stop();
    }
    if (strcmp((listMain -> lst)[0], "cd")==0) 
        flagCD = 1;
    return flagCD;
}

void phandler(int ggwp){
    wait(0);
}

char* concat(char *s1, char *s2) {

        size_t len1 = strlen(s1);
        size_t len2 = strlen(s2);                      

        char *result = malloc(len1 + len2 + 1);

        if (!result) {
            fprintf(stderr, "malloc() failed: insufficient memory!\n");
            return NULL;
        }

        memcpy(result, s1, len1);
        memcpy(result + len1, s2, len2 + 1);    

        return result;
    }

char *fromInttoStr(int n){
    char *buffer = malloc(5);

    int i = 0;
    int a;
    while (n != 0){
        a = n%10;
        if (a == 0) buffer = concat("0", buffer);
        if (a == 1) buffer = concat("1", buffer);
        if (a == 2) buffer = concat("2", buffer);
        if (a == 3) buffer = concat("3", buffer);
        if (a == 4) buffer = concat("4", buffer);
        if (a == 5) buffer = concat("5", buffer);
        if (a == 6) buffer = concat("6", buffer);
        if (a == 7) buffer = concat("7", buffer);
        if (a == 8) buffer = concat("8", buffer);
        if (a == 9) buffer = concat("9", buffer);
        n /= 10;
        i++;
    }
    
    return buffer;
    
}

void fromListToCmd(int i, cmd_inf *cmd){
    int j = 0;
    while (i<(listMain->sizelist)-1){
        if (!strcmp((listMain -> lst)[i], ">")){
            (cmd -> outfile) = (char*)malloc(strlen((listMain -> lst)[i+1])+1);
            strcpy(cmd -> outfile, (listMain -> lst)[i+1]);
            i+=2;
        }
        else if (!strcmp((listMain -> lst)[i], ">>")){
            (cmd -> outfileEND) = (char*)malloc(strlen((listMain -> lst)[i+1])+1);
            strcpy(cmd -> outfileEND, (listMain -> lst)[i+1]);
            i+=2;
        }    
        else if (!strcmp((listMain -> lst)[i], "<")){
            (cmd -> infile) = (char*)malloc(strlen((listMain -> lst)[i+1])+1);
            strcpy(cmd -> infile, (listMain -> lst)[i+1]);
            i+=2;
        }
            
        else if (!strcmp((listMain -> lst)[i], "&")){
            cmd -> backgrnd = 1;
            cmd -> next = (cmd_inf *)malloc(sizeof(cmd_inf));
            nullcmd(cmd -> next);
            fromListToCmd(i+1, cmd -> next);
            break;
        }
        else if (!strcmp((listMain -> lst)[i], "#")){
            break;
        }
        else if (!strcmp((listMain -> lst)[i], "exit")){
            stop();
        }
        else if (!strcmp((listMain -> lst)[i], "$HOME")){
            if (cmd -> argv == NULL){
                cmd -> argv = (char**)malloc(sizeof(char*) * (listMain->sizelist + 2));
            }
            (cmd -> argv)[j] = (char*)malloc(strlen(getenv("HOME")) + 2);
            strcpy((cmd -> argv)[j], getenv("HOME"));
            i++;
            j++;
            (cmd -> argv)[j]=NULL;
        }
        else if (!strcmp((listMain -> lst)[i], "$SHELL")){
            if (cmd -> argv == NULL){
                cmd -> argv = (char**)malloc(sizeof(char*) * (listMain->sizelist + 2));
            }
            int size = 256;
            char buf[size];
            (cmd -> argv)[j] = (char*)malloc(strlen(getcwd(buf,size)) + 2);
            strcpy((cmd -> argv)[j], getcwd(buf,size));
            i++;
            j++;
            (cmd -> argv)[j]=NULL;
        }
        else if (strcmp((listMain -> lst)[i], "$USER")==0){
            if (cmd -> argv == NULL){
                cmd -> argv = (char**)malloc(sizeof(char*) * (listMain->sizelist + 2));
            }
            (cmd -> argv)[j] = (char*)malloc(strlen(getenv("USER"))+2);
            strcpy((cmd -> argv)[j], getenv("USER"));
            i++;
            j++;
            (cmd -> argv)[j]=NULL;
        }
        else if (!strcmp((listMain -> lst)[i], "$EUID")){
            if (cmd -> argv == NULL){
                cmd -> argv = (char**)malloc(sizeof(char*) * (listMain->sizelist + 2));
            }
            (cmd -> argv)[j] = (char*)malloc(sizeof(uid_t) + 2);
            strcpy((cmd -> argv)[j], fromInttoStr((int)geteuid()));
            i++;
            j++;
            (cmd -> argv)[j]=NULL;
        }

        else if (!strcmp((listMain -> lst)[i], "|")){
            cmd -> pipe = (cmd_inf *)malloc(sizeof(cmd_inf));
            nullcmd(cmd -> pipe);
            fromListToCmd(i+1, cmd -> pipe);
            break;
        }
        else if (!strcmp((listMain -> lst)[i], ";")){
            cmd -> backgrnd = 0;
            cmd -> next = (cmd_inf *)malloc(sizeof(cmd_inf));
            nullcmd(cmd -> next);
            fromListToCmd(i+1, cmd -> next);
            break;
        }
        else {
            if (cmd -> argv == NULL){
                cmd -> argv = (char**)malloc(sizeof(char*) * (listMain->sizelist + 2));
            }
            (cmd -> argv)[j] = (char*)malloc(strlen((listMain -> lst)[i]));
            strcpy((cmd -> argv)[j], (listMain -> lst)[i]);
            i++;
            j++;
            (cmd -> argv)[j]=NULL;
        }
    }
}
// ls > Anton.txt | ls -l > Vlad.txt | echo Artem Samoillov | cat < DIMA.txt
void posledovatelno(cmd_inf *cmd){
    while (1){
        if ((cmd -> next) != NULL){
            //perror("posl cmd -> next");
            runCommand(cmd);
            cmd = cmd -> next;
        }
        else if ((cmd -> pipe) != NULL){
            //perror("posl cmd -> pipe");
            conveer(cmd);
            break;
        }
        else {
            //perror("posl last");
            runCommand(cmd);
            break;
        }
    }
}

void conveer(cmd_inf *cmd){
    
    int fd[2], in, out, next_in, i, count = 0;
    pipe(fd); out=fd[1]; next_in=fd[0];
    //perror("gg");
    if (fork()==0){
        close(next_in);
        dup2(out,1);
        close(out);
        if (cmd->argv != NULL) {
            if (cmd -> infile != NULL){
                fd[0] = open(cmd ->infile , O_RDONLY,0777);
                dup2(fd[0], 0);
                close(fd[0]);
            }
            if (cmd -> outfileEND != NULL){
                fd[1] = open(cmd -> outfileEND , O_CREAT | O_WRONLY | O_APPEND,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }
            if (cmd -> outfile != NULL){
                fd[1] = open(cmd ->outfile , O_CREAT | O_WRONLY,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }

            if (execvp((cmd -> argv)[0], cmd -> argv) == -1){
                printf("ERROR %s : команда не найдена\n", cmd -> argv[0]);
            }
            exit(0);
        }
    }
    cmd = cmd -> pipe;
    in = next_in;
    while ((cmd -> pipe) != NULL){
        count ++;
        close(out);
        pipe(fd);
        out = fd[1];
        next_in = fd[0];
        //perror("gg");
        
        if(fork()==0){
            dup2(in,0);
            close(in);
            dup2(out,1);
            close(out);
            //runCommand(cmd);
            if (cmd->argv != NULL) {
                if (cmd -> infile != NULL){
                    fd[0] = open(cmd ->infile , O_RDONLY,0777);
                    dup2(fd[0], 0);
                    close(fd[0]);
                }
                if (cmd -> outfileEND != NULL){
                    fd[1] = open(cmd -> outfileEND , O_CREAT | O_WRONLY | O_APPEND,0777);
                    dup2(fd[1], 1);
                    close(fd[1]);
                }
                if (cmd -> outfile != NULL){
                    fd[1] = open(cmd ->outfile , O_CREAT | O_WRONLY,0777);
                    dup2(fd[1], 1);
                    close(fd[1]);
                }

                if (execvp((cmd -> argv)[0], cmd -> argv) == -1){
                    printf("ERROR %s : команда не найдена\n", cmd -> argv[0]);
                }
                exit(0);
            }
        }
        close(in);
        in = next_in;
        cmd = cmd -> pipe;
    }
    close(out);
    //perror("gg");
    if (fork()==0){
        dup2(in,0);
        close(in);
        if (cmd->argv != NULL) {
            if (cmd -> infile != NULL){
                fd[0] = open(cmd ->infile , O_RDONLY,0777);
                dup2(fd[0], 0);
                close(fd[0]);
            }
            if (cmd -> outfileEND != NULL){
                fd[1] = open(cmd -> outfileEND , O_CREAT | O_WRONLY | O_APPEND,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }
            if (cmd -> outfile != NULL){
                fd[1] = open(cmd ->outfile , O_CREAT | O_WRONLY,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }

            if (execvp((cmd -> argv)[0], cmd -> argv) == -1){
                printf("ERROR %s : команда не найдена\n", cmd -> argv[0]);
            }
            exit(0);
        }
    }
    close(in);
    
    for( i = 0; i < count + 2; i++) {
        //perror("lina");
        wait(NULL);
        //perror("working");
    }
    if ((cmd -> next) != NULL) posledovatelno(cmd -> next);
}

void runCommand(cmd_inf *cmd){
    int fd[2];
    if (cmd->argv != NULL) {
        if (fork() == 0){
            if (cmd -> infile != NULL){
                fd[0] = open(cmd ->infile , O_RDWR,0777);
                dup2(fd[0], 0);
                close(fd[0]);
            }
            if (cmd -> outfileEND != NULL){
                fd[1] = open(cmd -> outfileEND , O_CREAT | O_RDWR | O_APPEND,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }
            if (cmd -> outfile != NULL){
                fd[1] = open(cmd ->outfile , O_CREAT | O_RDWR | O_TRUNC,0777);
                dup2(fd[1], 1);
                close(fd[1]);
            }

            if (execvp((cmd -> argv)[0], cmd -> argv) == -1){
                printf("ERROR %s : команда не найдена\n", cmd -> argv[0]);
            }
            exit(0);
        }
        if ( !(cmd -> backgrnd) ) wait(NULL);
    }
    
}
