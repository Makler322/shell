prog: task5.c moduleB.o moduleA.o
	gcc -g -Wall task5.c moduleB.o moduleA.o -o prog
moduleB.o: moduleB.c module.h
	gcc -g -Wall -c moduleB.c -o moduleB.o
moduleA.o: moduleA.c module.h
	gcc -g -Wall -c moduleA.c -o moduleA.o